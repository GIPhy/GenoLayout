#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  GenoLayout: linear maps (showing conserved orthologous fragments) between genomes                         #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2022,2023 Institut Pasteur"                                                      #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Bioinformatics and Biostatistics Hub                              research.pasteur.fr/en/team/hub-giphy  #
#   Dpt. Biologie Computationnelle            research.pasteur.fr/team/bioinformatics-and-biostatistics-hub  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=1.5.230220ac                                                                                       #
# + updating finalizers for BLAST+ version >= 2.13.0                                                         #
#                                                                                                            #
#  VERSION=1.4.220405ac                                                                                      #
# + GSCALE is computed to always obtain SVG files of fixed width 5000 px                                     #
#                                                                                                            #
# VERSION=1.3.220307ac                                                                                       #
# + default -k 25 -d 1                                                                                       #
# + modified computation of the default dimensions                                                           #
#                                                                                                            #
# VERSION=1.2.220306ac                                                                                       #
# + twice more lines using overlapping fragments                                                             #
#                                                                                                            #
# VERSION=1.1.220304ac                                                                                       #
# + fixed bug when computing the genome lengths                                                              #
# + fixed rounding problem when computing figure dimensions                                                  #
#                                                                                                            #
# VERSION=1.0.220222ac                                                                                       #
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
#  ================                                                                                          #
#  = INSTALLATION =                                                                                          #
#  ================                                                                                          #
#                                                                                                            #
#  Just give the execute permission to the script wgetENAHTS.sh with the following command line:             #
#                                                                                                            #
#   chmod +x GenoLayout.sh                                                                                   #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ==============================                                                                             #
# = STATIC FILES AND CONSTANTS =                                                                             #
# ==============================                                                                             #
#                                                                                                            #
# -- PWD: directory containing the current script ---------------------------------------------------------  #
#                                                                                                            #
  PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)";
#                                                                                                            # 
# -- constants --------------------------------------------------------------------------------------------  #
#                                                                                                            #
  NA="._N.o.N._.A.p.P.l.I.c.A.b.L.e_.";
#                                                                                                            #
# -- SVG colors -------------------------------------------------------------------------------------------  #
#                                                                                                            #
  CSAFE="aqua|black|blue|cyan|fuchsia|gray|green|grey|lime|magenta|maroon|navy|olive|purple|red|silver|teal|white|yellow";
  CA="aliceblue|antiquewhite|aquamarine|azure";
  CB="beige|bisque|blanchedalmond|blueviolet|brown|burlywood";
  CC="cadetblue|chartreuse|chocolate|coral|cornflowerblue|cornsilk|crimson";
  CD="deeppink|deepskyblue|dimgray|dimgrey|dodgerblue";
  CDARK="darkblue|darkcyan|darkgoldenrod|darkgray|darkgreen|darkgrey|darkkhaki|darkmagenta|darkolivegreen|darkorange|darkorchid|darkred|darksalmon|darkseagreen|darkslateblue|darkslategray|darkslategrey|darkturquoise|darkviolet";
  CF="firebrick|floralwhite|forestgreen";
  CG="gainsboro|ghostwhite|gold|goldenrod|greenyellow";
  CH="honeydew|hotpink";
  CI="indianred|indigo|ivory";
  CK="khaki";
  CL="lavender|lavenderblush|lawngreen|lemonchiffon|limegreen|linen";
  CLIGHT="lightblue|lightcoral|lightcyan|lightgoldenrodyellow|lightgray|lightgreen|ightgrey|lightpink|lightsalmon|lightseagreen|lightskyblue|lightslategray|lightslategrey|lightsteelblue|lightyellow";
  CM="midnightblue|mintcream|mistyrose|moccasin";
  CMEDIUM="mediumaquamarine|mediumblue|mediumorchid|mediumpurple|mediumseagreen|mediumslateblue|mediumspringgreen|mediumturquoise|mediumvioletred";
  CN="navajowhite";
  CO="oldlace|olivedrab|orange|orangered|orchid";
  CP="papayawhip|peachpuff|peru|pink|plum|powderblue";
  CPALE="palegoldenrod|palegreen|paleturquoise|palevioletred";
  CR="rosybrown|royalblue";
  CS="saddlebrown|salmon|sandybrown|seagreen|seashell|sienna|skyblue|slateblue|slategray|slategrey|snow|springgreen|steelblue";
  CT="tan|thistle|tomato|turquoise";
  CV="violet";
  CW="wheat|whitesmoke";
  CY="yellowgreen";
  SVGCOLORS="$CSAFE|$CA|$CB|$CC|$CD|$CDARK|$CF|$CG|$CH|$CI|$CK|$CL|$CLIGHT|$CM|$CMEDIUM|$CN|$CO|$CP|$CPALE|$CR|$CS|$CT|$CV|$CW|$CY"
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
#  ================                                                                                          #
#  = FUNCTIONS    =                                                                                          #
#  ================                                                                                          #
#                                                                                                            #
# = echoxit() ============================================================================================   #
#   prints in stderr the specified error message $1 and next exit 1                                          #
#                                                                                                            #
echoxit() {
  echo "$1" >&2 ; exit 1 ;
}    
#                                                                                                            #
# = randfile =============================================================================================   #
#   creates and returns a random file name that does not exist from the specified basename $1                #
#                                                                                                            #
randfile() {
  local rdf="$(mktemp $1.XXXXXXXXX)";
  echo $rdf ;
}
#                                                                                                            #
# = mandoc() =============================================================================================   #
#   prints the doc                                                                                           #
#                                                                                                            #
mandoc() {
  cat <<EOF

 USAGE:  GenoLayout [OPTIONS] <fasta1> <fasta2> <fasta3> [<fasta4> ...]

 OPTIONS:
  -o <file>    SVG outfile name             (mandatory)
  -w <int>     window size          (bp; default: 1000)
  -k <int>     blastn word size       (bp; default: 25)
  -j <int>     draw every j lines          (default: 1)
  -a <string>  font color         (default: ghostwhite)
  -b <string>  box color        (default: midnightblue)
  -c <string>  line color             (default: tomato)
  -d <int>     contig delimiter width  (px; default: 1)
  -s <real>    span (width) factor       (default: 1.0)
  -x <int>     box height           (px; default: auto)
  -y <int>     gap height           (px; default: auto)
  -z <int>     font size            (px; default: auto)
  -t <int>     number of threads           (default: 2)
  -h           prints this help and exits

EOF
}
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- gawk -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GAWK_BIN=gawk;
  [ ! $(command -v $GAWK_BIN) ] && echoxit "no $GAWK_BIN detected" ;
  GAWK_STATIC_OPTIONS="";     
  GAWK="$GAWK_BIN $GAWK_STATIC_OPTIONS";
  TAWK="$GAWK -F\\t";
#                                                                                                            #
# -- NCBI BLAST+ (version >= 2.12.0) ----------------------------------------------------------------------  #
#                                                                                                            #
  export BLAST_USAGE_REPORT=false;         ## from https://www.ncbi.nlm.nih.gov/books/NBK569851/
#                                                                                                            #
# -- makeblastdb                                                                                             #
#                                                                                                            #
  MKBDB_BIN=makeblastdb;
  [ ! $(command -v $MKBDB_BIN) ] && echoxit "no $MKBDB_BIN detected" ;
  version="$($MKBDB_BIN -version | $GAWK '(NR==2){print$3}' | $GAWK -F"." '($1>=2&&$2>=12)')";
  [ -z "$version" ] && echoxit "incorrect version: $MKBDB_BIN" ;
  MKBDB_STATIC_OPTIONS="-input_type fasta";     
  MKBDB="$MKBDB_BIN $MKBDB_STATIC_OPTIONS";
  MKBNDB="$MKBDB -dbtype nucl";
#                                                                                                            #
# -- BLAST                                                                                                   #
#                                                                                                            #
  BLAST_STATIC_OPTIONS="-evalue 1e-5 -soft_masking false -max_target_seqs 1 -subject_besthit -max_hsps 1";
#                                                                                                            #
# -- blastn                                                                                                  #
#                                                                                                            #
  BLASTN_BIN=blastn;
  [ ! $(command -v $BLASTN_BIN) ] && echoxit "no $BLASTN_BIN detected" ;
  version="$($BLASTN_BIN -version | $GAWK '(NR==2){print$3}' | $GAWK -F"." '($1>=2&&$2>=12)')";
  [ -z "$version" ] && echoxit "incorrect version: $BLASTN_BIN" ;
  BLASTN_STATIC_OPTIONS="-task blastn -perc_identity 30 -dust no";
  BCOST="-reward 1 -penalty -1 -gapopen 5 -gapextend 2 -xdrop_ungap 20 -xdrop_gap 150 -xdrop_gap_final 100";
  BLASTN="$BLASTN_BIN $BLAST_STATIC_OPTIONS $BLASTN_STATIC_OPTIONS $BCOST";
#                                                                                                            #
##############################################################################################################


##############################################################################################################
####                                                                                                      ####
#### INITIALIZING PARAMETERS AND READING OPTIONS                                                          ####
####                                                                                                      ####
##############################################################################################################

echo -e "\n\033[1mGenoLayout v$VERSION            $COPYRIGHT\033[0m";

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

export LC_ALL=C ;

OUTFILE="$NA";       # -o
WSIZE=1000;          # -w 1000
KMER=25;             # -k
JUMP=1;              # -j
FCOL="ghostwhite";   # -a
RCOL="midnightblue"; # -b
LCOL="tomato";       # -c
DELW=1;              # -d
SPAN=1.0;            # -s
HREC="$NA";          # -x 200
HGAP="$NA";          # -y 1000
FSIZ="$NA";          # -z 150
NTHREADS=2;          # -t

while getopts o:w:k:j:a:b:c:d:s:x:y:z:t:h option
do
  case $option in
  o)  OUTFILE="$OPTARG" ;;
  w)  WSIZE=$OPTARG     ;;
  k)  KMER=$OPTARG      ;;
  j)  JUMP=$OPTARG      ;;
  a)  FCOL=$OPTARG      ;;
  b)  RCOL=$OPTARG      ;;
  c)  LCOL=$OPTARG      ;;
  d)  DELW=$OPTARG      ;;
  s)  SPAN=$OPTARG      ;;
  x)  HREC=$OPTARG      ;;
  y)  HGAP=$OPTARG      ;;
  z)  FSIZ=$OPTARG      ;;
  t)  NTHREADS=$OPTARG  ;;
  h)  mandoc ;  exit 0  ;;
  :)  mandoc ;  exit 1  ;;
  \?) mandoc ;  exit 1  ;;
  esac
done

echo                                >&2 ;

[ "$OUTFILE" == "$NA" ]                               && echoxit "output file not specified (option -o)" ;

[[ $WSIZE =~ ^[0-9]+$ ]]                              || echoxit "incorrect value (option -w): $WSIZE" ; 
[ $WSIZE -lt 100 ]                                    && echoxit "window size should at least 100 (option -w)" ;
[ $WSIZE -gt 50000 ]                                  && echoxit "window size should at most 50000 (option -w)" ;

echo "window size     (-w)  $WSIZE bp" >&2 ;

[[ $KMER =~ ^[0-9]+$ ]]                               || echoxit "incorrect value (option -k): $KMER" ; 
[ $KMER -lt 11 ]                                      && echoxit "k-mer should at least 11 (option -k)" ;
[ $KMER -gt $(( $WSIZE / 2 )) ]                       && echoxit "k-mer should at most half the window size (option -k)" ;
BLASTN="$BLASTN -word_size $KMER";

echo "k-mer length    (-k)  $KMER bp"  >&2 ;

[[ $JUMP =~ ^[0-9]+$ ]]                               || echoxit "incorrect value (option -j): $JUMP" ; 
[ $JUMP -lt 1 ] && JUMP=1;

[ $(grep -c -E "^($SVGCOLORS)$" <<<"$FCOL") -ne 1 ]   && echoxit "incorrect color (option -a): $FCOL" ;
[ $(grep -c -E "^($SVGCOLORS)$" <<<"$RCOL") -ne 1 ]   && echoxit "incorrect color (option -b): $RCOL" ;
[ $(grep -c -E "^($SVGCOLORS)$" <<<"$LCOL") -ne 1 ]   && echoxit "incorrect color (option -c): $LCOL" ;

[[ $SPAN =~ ^[0-9]+\.[0-9]+$ ]]                       || echoxit "incorrect value (option -s): $SPAN" ;

[[ $DELW =~ ^[0-9]+$ ]]                               || echoxit "incorrect value (option -d): $DELW" ; 

if [ "$HGAP" != "$NA" ]; then [[ $HGAP =~ ^[0-9]+$ ]] || echoxit "incorrect value (option -x): $HGAP" ; fi
if [ "$HREC" != "$NA" ]; then [[ $HREC =~ ^[0-9]+$ ]] || echoxit "incorrect value (option -y): $HREC" ; fi
if [ "$FSIZ" != "$NA" ]; then [[ $FSIZ =~ ^[0-9]+$ ]] || echoxit "incorrect value (option -z): $FSIZ" ; fi

[[ $NTHREADS =~ ^[0-9]+$ ]]                           || echoxit "incorrect value (option -t): $NTHREADS" ; 
[ $NTHREADS -lt 1 ] && NTHREADS=1;
[ $NTHREADS -gt 1 ] && BLASTN="$BLASTN -num_threads $NTHREADS -mt_mode 1";

echo "no. threads     (-t)  $NTHREADS" >&2 ;

shift "$(( $OPTIND - 1 ))"

## tmp files #################################################################################################
FRAG1=$(randfile $OUTFILE.frag1);
FRAG2=$(randfile $OUTFILE.frag2);
OUT1=$(randfile $OUTFILE.out1);
OUT2=$(randfile $OUTFILE.out2);
TMP1=$(randfile $OUTFILE.tmp1);
TMP2=$(randfile $OUTFILE.tmp2);
INFO1=$(randfile $OUTFILE.info1);
INFO2=$(randfile $OUTFILE.info2);

## defining traps ############################################################################################
finalizer() {
  rm -f $FRAG1 $FRAG1.ndb $FRAG1.nhr $FRAG1.nin $FRAG1.njs $FRAG1.not $FRAG1.nsq $FRAG1.ntf $FRAG1.nto ;
  rm -f $FRAG2 $FRAG2.ndb $FRAG2.nhr $FRAG2.nin $FRAG2.njs $FRAG2.not $FRAG2.nsq $FRAG2.ntf $FRAG2.nto ;
  rm -f $OUT1 $OUT2 $TMP1 $TMP2 $INFO1 $INFO2;
}
echoxit() {
  finalizer ;
  echo -e "\n$1" >&2 ;
  exit 1 ;
}    
trap 'finalizer;exit 1'  INT ERR TERM ;


##############################################################################################################
####                                                                                                      ####
#### PROCESSING FIRST FILE                                                                                ####  
####                                                                                                      ####
##############################################################################################################

FASTA2=$1;

N=1;
echo -n "infile $N              $FASTA2 " >&2 ;

[ ! -e $FASTA2 ] && echoxit "file not found: $FASTA2" ;
[   -d $FASTA2 ] && echoxit "not a file: $FASTA2" ;
[ ! -s $FASTA2 ] && echoxit "empty file: $FASTA2" ;
[ ! -r $FASTA2 ] && echoxit "no read permission: $FASTA2" ;

## genome info ###############################################################################################
lgt2=$(grep -v "^>" $FASTA2 | tr -d '[:cntrl:]' | wc -c);
echo -n "$(basename ${FASTA2%.*}) $lgt2"       >  $INFO1 ;                                   echo -n "." >&2 ;
$GAWK '/^>/{if(s!="")printf" "length(s);s="";}
           {s=s$0}
       END {print" "length(s)}' $FASTA2        >> $INFO1 ;                                   echo -n "." >&2 ;

## cutting sequences into consecutive ws-long fragments ######################################################
lgt=$(( $WSIZE / 2 ));
$GAWK '/^>/{if(++n>1)printf"@";next}
            {printf$0}' $FASTA2 |
  tr '[a-z]' '[A-Z]' |
    tr -d 'N X' |
      fold -w $lgt | 
        $GAWK -v hw=$lgt 'BEGIN  {x=1000000000}
                          (NR==1){fpre=$1;next}
                                 {fcur=$1;
                                  frag=fpre""fcur;
                                  x+=hw;
                                  if(index(frag,"@")==0 && length(frag)>=hw){
                                    print">"x;
                                    print frag;
                                  }
                                  fpre=fcur;
                                 }' > $FRAG2 ;                                               echo -n "." >&2 ;

echo " [ok]" >&2 ;

##############################################################################################################
####                                                                                                      ####
#### COMPARING GENOMES                                                                                    ####  
####                                                                                                      ####
##############################################################################################################

shift 1 ;

for FASTA in $@
do
  if [ ! -e $FASTA ]; then echo "file not found: $FASTA"     >&2 ; continue; fi 
  if [   -d $FASTA ]; then echo "not a file: $FASTA"         >&2 ; continue; fi 
  if [ ! -s $FASTA ]; then echo "empty file: $FASTA"         >&2 ; continue; fi 
  if [ ! -r $FASTA ]; then echo "no read permission: $FASTA" >&2 ; continue; fi

  FASTA1=$FASTA2;
  lgt1=$lgt2;
  mv $FRAG2 $FRAG1; touch $FRAG2 ;
  FASTA2=$FASTA;

  N=$(( $N + 1 ));
  echo -n "infile $N              $FASTA2 " >&2 ;
 
  ## formatting fragments ####################################################################################
  $MKBNDB -in $FRAG1 &>/dev/null ;  
  
  ## genome info #############################################################################################
  lgt2=$(grep -v "^>" $FASTA2 | tr -d '[:cntrl:]' | wc -c);
  echo -n "$(basename ${FASTA2%.*}) $lgt2"       >> $INFO1 ;
  $GAWK '/^>/{if(s!="")printf" "length(s);s="";}
             {s=s$0}
         END {print" "length(s)}' $FASTA2        >> $INFO1 ;

  ## cutting sequences into consecutive ws-long fragments ####################################################
  lgt=$(( $WSIZE / 2 ));
  $GAWK '/^>/{if(++n>1)printf"@";next}
              {printf$0}' $FASTA2 |
    tr '[a-z]' '[A-Z]' |
      tr -d 'N X' |
        fold -w $lgt | 
          $GAWK -v hw=$lgt 'BEGIN  {x=1000000000}
                            (NR==1){fpre=$1;next}
                                   {fcur=$1;
                                    frag=fpre""fcur;
                                    x+=hw;
                                    if(index(frag,"@")==0 && length(frag)>=hw){
                                      print">"x;
                                      print frag;
                                    }
                                    fpre=fcur;
                                   }' > $FRAG2 ;                                             echo -n "." >&2 ;

  ## formatting fragments ####################################################################################
  $MKBNDB -in $FRAG2 &>/dev/null ;  
  
  ## blastn similarity search between fragments ##############################################################
  #                                       output fields: 1----- 2--- 3----- 4--- 5----- 6--- 7----- 8--- 9-----
  $BLASTN -query $FRAG1 -db $FRAG2 -out $TMP1 -outfmt '6 qseqid qlen qstart qend sseqid slen sstart send nident' 2>/dev/null ; echo -n "." >&2 ;
  $BLASTN -query $FRAG2 -db $FRAG1 -out $TMP2 -outfmt '6 qseqid qlen qstart qend sseqid slen sstart send nident' 2>/dev/null ; echo -n "." >&2 ;

  ## filtering out BLAST hits ################################################################################
  #  + nident/qlen < 0.3
  #  + nident/slen < 0.3
  #  + (qend-qstart+1)/qlen < 0.35
  #  + (|send-sstart|+1)/slen < 0.35
  $TAWK 'function abs(x){return(x+=0)<0?-x:x}
         ($1==q || $9/$2<0.3 || $9/$6<0.3 || ($4-$3+1)/$2<0.35 || (abs($8-$7)+1)/$6<0.35){next} {q=$1;print}' $TMP1 > $OUT1 ;
  $TAWK 'function abs(x){return(x+=0)<0?-x:x}
         ($1==q || $9/$2<0.3 || $9/$6<0.3 || ($4-$3+1)/$2<0.35 || (abs($8-$7)+1)/$6<0.35){next} {q=$1;print}' $TMP2 > $OUT2 ;
 
  ## assessing reciprocal BLAST hits (RBH) ###################################################################
  #  sorting OUT2 according to the decreasing order of the subject ids (field 5)
  sort -k5,5 -k9rg,9 $OUT2 > $TMP2 ; mv $TMP2 $OUT2 ; touch $TMP2 ;
  # head $OUT1 ; head $OUT2 ; join -t$'\t' -1 1 -2 5 $OUT1 $OUT2 | head ;
  # joining OUT1 and OUT2 to obtain RBHs (i.e. resulting fields $5 and $10 should be identical)
  join -t$'\t' -1 1 -2 5 $OUT1 $OUT2 | $TAWK '($5==$10)' | cut -f1-9 > $TMP1 ;

  ## gathering coordinates ###################################################################################
  echo "#"                                                >> $INFO2 ;
  $TAWK '{print($1-1000000000)"\t"($5-1000000000)}' $TMP1 >> $INFO2 ;
  
  echo " [ok]" >&2 ;

  rm -f $FRAG1.ndb $FRAG1.nhr $FRAG1.nin $FRAG1.njs $FRAG1.not $FRAG1.nsq $FRAG1.ntf $FRAG1.nto ;
  rm -f $FRAG2.ndb $FRAG2.nhr $FRAG2.nin $FRAG2.njs $FRAG2.not $FRAG2.nsq $FRAG2.ntf $FRAG2.nto ;
done

rm -f $FRAG1 $FRAG2 ;
rm -f $OUT1  $TMP1 ;
rm -f $OUT2  $TMP2 ;


##############################################################################################################
####                                                                                                      ####
#### DRAWING FIGURE                                                                                       ####  
####                                                                                                      ####
##############################################################################################################

## format2svg ################################################################################################
BOXCOLOR="fill=\"$RCOL\"";

## genome stats ##############################################################################################
NSEQ=$(cat $INFO1 | wc -l);
GMAX=$($GAWK '{max=(max<$2)?$2:max}END{print max}' $INFO1);

echo "no. files             $NSEQ"      >&2 ;
echo "max. seq. lgt         $GMAX bp"   >&2 ;

## enlarging/shrinking using the span factor #################################################################
#  + every box width will be divided by WSCALE; default: WSCALE = WSIZE
#  + the SPAN factor can be used to rescale the overall width
WSCALE=$(bc <<<"(0.5*$WSIZE/(2*$SPAN))/1");

## setting vertical dimensions ###############################################################################
[ "$HREC" == "$NA" ] && HREC=$(( $GMAX / (10 * $WSIZE) ));
[ "$HGAP" == "$NA" ] && HGAP=$(( 5 * $HREC ));
[ "$FSIZ" == "$NA" ] && FSIZ=$(( 3 * $HREC / 4 ));

echo "span factor     (-s)  $SPAN"      >&2 ;
echo "box height      (-x)  $HREC px"   >&2 ;
echo "gap height      (-y)  $HGAP px"   >&2 ;
echo "font size       (-z)  $FSIZ px"   >&2 ;

## setting parameters to gawk ################################################################################
GAWK="$GAWK -v ws=$WSCALE -v hr=$HREC -v hg=$HGAP -v fs=$FSIZ -v dw=$DELW -v jump=$JUMP";

## setting figure dimensions #################################################################################
WIDTH=$(( $GMAX / $WSCALE + 100 ));
HEIGTH=$(( $NSEQ * ($HGAP + $HREC) - $HGAP + 100 ));

echo "svg height            $HEIGTH px" >&2 ;
echo "svg width             $WIDTH px"  >&2 ;
echo "delimiter width (-d)  $DELW px"   >&2 ;
echo "jump lines      (-j)  $JUMP"      >&2 ;
echo "font color      (-a)  $FCOL"      >&2 ;
echo "box color       (-b)  $RCOL"      >&2 ;
echo "line color      (-c)  $LCOL"      >&2 ;
echo "outfile         (-o)  $OUTFILE"   >&2 ;

## shrinking the whole file for web browsers #################################################################
#  + GSCALE is the scale factor for the overall SVG transform
GSCALE=$(bc -l <<<"scale=8;5000/$WIDTH" | sed 's/^\./0\./');
GWIDTH=$(bc <<<"(0.5+$WIDTH*$GSCALE)/1");
GHEIGTH=$(bc <<<"(0.5+$HEIGTH*$GSCALE)/1");

## writing svg file ##########################################################################################
{ echo -e "<svg version=\"1.1\" width=\"$GWIDTH\" height=\"$GHEIGTH\" xmlns=\"http://www.w3.org/2000/svg\">\n" ;

  echo -e " <style>" ;
  echo -e "  text {" ;
  echo -e "   font-family: Arial, Helvetica, sans-serif;" ;
  echo -e "   text-anchor: left;" ;
  echo -e "   fill: $FCOL;" ;
  echo -e "  }" ;
  echo -e "  line {" ;
  echo -e "   stroke: $LCOL;" ;
  echo -e "   stroke-linecap: round;" ;
  echo -e "   stroke-width: 1;" ;
  echo -e "  }" ;
  echo -e " </style>\n" ;
  
  echo -e " <g transform=\"scale($GSCALE)\">\n" ;

  echo -e "  <!-- background -->\n" ;

  echo -e "  <rect width=\"$WIDTH\" height=\"$HEIGTH\" fill=\"white\"/>\n" ;

  echo -e "  <!-- genomes boxes -->\n" ;

  $GAWK 'BEGIN{y=50-hg}
              {y+=(hg+hr); 
               if(hr>0){
                 print "  <rect x=\"50\" y=\""(y-hr)"\" width=\""int($2/ws)"\" height=\""hr"\" @RC@/>" ;
                 print "" ;
               }
               s=50;field=2;
               while(++field<NF){
                 s+=int($field/ws);
                 print "  <rect x=\""(s-int(dw/2))"\" y=\""(y-hr)"\" width=\""dw"\" height=\""hr"\" fill=\"white\"/>";
                 print "" ;
               }
               if(fs>0){
                 print "  <text x=\"55\" y=\""(y-int(hr/2)+int(fs/3))"\" font-size=\""fs"\">&#160; "$1"</text>";
                 print "" ;
               }
              }' $INFO1 ;

  echo -e "  <!-- lines -->\n" ;

  $GAWK 'BEGIN           {y1=50-hg}
         /^#/            {y1+=(hg+hr);
                          y2=y1+hg;
                          nl=-1;
                          next;
                         }
         ((++nl)%jump==0){print "  <line x1=\""int(50+$1/ws)"\" x2=\""int(50+$2/ws)"\" y1=\""y1"\" y2=\""y2"\"/>";
                          print "";}' $INFO2 ;

  echo -e " </g>\n" ;
  echo "</svg>" ;
} | sed "s/@RC@/$BOXCOLOR/g" > $OUTFILE


##############################################################################################################
####                                                                                                      ####
#### EXITING                                                                                              ####  
####                                                                                                      ####
##############################################################################################################

rm -r $INFO1 $INFO2 ;

echo  >&2 ;

exit ;



